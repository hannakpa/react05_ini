import './App.css';
import React from 'react';
import Circulo from './Circulo';
import Container from 'react-bootstrap/Container';
import Contador from './Contador';
import Like from './Like';
import Numeros from './Numeros';
import Foco from './Foco';


function App() {
 
  return (
    <div className="App mt-5">
      <Container>
    
      <Circulo />
      <Circulo />
      <Circulo />
      <Circulo />
      <Circulo />
      <Contador />
      </Container>
      <Like />

      <Numeros />

      <Foco />
     
    </div>
  );
}

export default App;
