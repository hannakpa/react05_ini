import { useState } from 'react';
import './foco.css';


function Foco (){


    let bombillaOff = <img className='fondo' src='./img/off.jpg' alt="bombilla off" />;
    let interruptorOff= <img className='encima' src='./img/boff.jpg' alt="interruptor off" />;
    let bombillaOn= <img className='fondo' src='./img/on.jpg' alt="bombilla on" />;
    let interruptorOn= <img className='encima' src='./img/bon.jpg' alt="interruptor on" />;
    let foco=bombillaOn;
    let interruptor=interruptorOn;


    const [activo, setActivo] = useState(false);
    if (activo===true){
        foco=bombillaOff;
        interruptor=interruptorOff;
    }


    return(
        
        
        <div className='caja-img'>{foco}<div onClick={()=> setActivo(!activo)}>{interruptor}</div></div>
       

        
    )
}

export default Foco