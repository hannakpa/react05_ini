import { ListGroup, Button } from 'react-bootstrap';
import { FaBackward } from 'react-icons/fa';
import { FaForward } from 'react-icons/fa';
import { useState } from 'react';
import './numeros.css';


  function Numeros() {

     const [numero, setNumero] = useState(0); //la variable que va a cambiar. NUMERO


     //FUNCIONES DE LOS ONCLICK
     // FUNCION DE ONCLICK(DISMINUIR)
     function disminuir(){
         if(numero>0){
             setNumero(numero-1);
         }
     }
     
     //FUNCION DE ONCLICK(AUMENTAR)
     function aumentar(){
        if (numero<15){
            setNumero(numero+1);
        }
     }


        return (
            <div className="box-center">
            <ListGroup horizontal>
                <ListGroup.Item><Button onClick={disminuir}><FaBackward /></Button></ListGroup.Item>
                <ListGroup.Item onClick={()=>alert(`Este numero es ${numero+1}`)}>{numero+1}</ListGroup.Item>
                <ListGroup.Item onClick={()=>alert(`Este numero es ${numero+2}`)}>{numero+2}</ListGroup.Item>
                <ListGroup.Item onClick={()=>alert(`Este numero es ${numero+3}`)}>{numero+3}</ListGroup.Item>
                <ListGroup.Item onClick={()=>alert(`Este numero es ${numero+4}`)}>{numero+4}</ListGroup.Item>
                <ListGroup.Item onClick={()=>alert(`Este numero es ${numero+5}`)}>{numero+5}</ListGroup.Item>
                <ListGroup.Item><Button onClick={aumentar}><FaForward /></Button></ListGroup.Item>
            </ListGroup>
            </div>
        )
    }

export default Numeros



