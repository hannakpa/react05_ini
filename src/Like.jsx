
import { FaThumbsUp } from 'react-icons/fa';
import { FaThumbsDown } from 'react-icons/fa';
import { useState } from 'react';

function Like(){


const [activo, setActivo] = useState(false);

let dedoArriba = <FaThumbsUp />;
let dedoAbajo = <FaThumbsDown />
let dedo = dedoArriba;

if (activo ===true){
    dedo = dedoAbajo;
}


    return (
        <div>
              
            <i onClick={() => setActivo(!activo)}>{dedo}</i>         
        </div>
    )
    
};

export default Like