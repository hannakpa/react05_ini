import {InputGroup, Button, FormControl} from 'react-bootstrap';
import './cantidad.css';
import { useState } from 'react';



function Contador(){
  const [cantidad, setCantidad] = useState(0);


  function clicarMas (){
    if (cantidad<10){
      setCantidad(cantidad+1);
    }
    
  }

  function clicarMenos (){
    if (cantidad>0){
    setCantidad(cantidad-1); 
    }
}



    return (
      <InputGroup>
        <Button onClick={clicarMenos} > - </Button> 
        <FormControl disabled="disabled" aria-label="Text input" value={cantidad}  />
        <Button onClick={clicarMas} > + </Button>
      </InputGroup>
    )
}

export default Contador